var http    = require('http').createServer();
var io      = require('socket.io')(http);
var request = require('request');
http.listen(1234);

var headers = {
    'User-Agent'  : 'Super Agent/0.0.1',
    'Content-Type': 'application/x-www-form-urlencoded'
}

// Configure the request
var options = {
    url: 'http://localhost:3000/request/',
    method: 'POST',
    headers: headers,
    form: {}
}

_users = {}; //obj {socket.id : MongoId}
_ids   = []; //array ['MongoId' => socket.id]

io.sockets.on('connection', function(socket)
{
    //userのオン・オフライン
    socket.on('login', function(data)
    {
        _users[socket.id] = data;
        _ids[data]        = socket.id;

        io.sockets.emit('login_info', _users);
    });

    // メッセージ送信
    socket.on('C_to_S_message', function (data)
    {
        options.form = data;

        //fuelにPOSTリクエスト
        request(options, function (error, response, body)
        {
            if (!error && response.statusCode == 200)
            {
                body = JSON.parse(body);

                //自分・個別宛て
                if(typeof body.to !== 'undefined')
                {
                    //相手
                    if (typeof _ids[body.to] !== 'undefined')
                    {
                        io.sockets.to(_ids[body.to]).emit('S_to_C_message', body);
                    }

                    //自分
                    if (typeof _ids[body.owner] !== 'undefined')
                    {
                        //自分宛てだとbody.toですでに出てるのでなし
                        if(body.owner !== body.to)
                            io.sockets.to(_ids[body.owner]).emit('S_to_C_message', body);
                    }
                }
                else if(typeof body.to === 'undefined')
                {
                    //main room
                    io.sockets.emit('S_to_C_message', body);
                }           
            }
        });
    });

    socket.on('disconnect', function() 
    {
        delete _users[socket.id];

        //みんなに知らせる
        io.sockets.emit('login_info', _users);
    });
});