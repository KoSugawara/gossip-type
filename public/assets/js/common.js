var checkPermission = function()
{
    var node = document.getElementById('notify-permission');
    if(!notify.isSupported)
    {
        node.innerHTML = 'このブラウザではデスクトップ通知が許可されていません';
    }
    else
    {
        $('#push').removeClass('hide');
        switch(notify.permissionLevel())
        {
            case notify.PERMISSION_GRANTED:
                //node.innerHTML = 'デスクトップ通知を使用することができます。';
                $('#push').addClass('hide');
            break;
            case notify.PERMISSION_DEFAULT:
                node.innerHTML = '「通知許可を求める」から、通知を許可して下さい。';
            break;
            case notify.PERMISSION_DENIED:
                node.innerHTML = 'デスクトップ通知が許可されていません。ブラウザの設定を確認して下さい。';
            break;
        }
    }
}

var requestPermission = function()
{
    notify.requestPermission();
}

var show = function(title, body)
{
    notify.config({autoClose: 5000});
    notify.createNotification(title, { body: body, icon: 'drops.png' });
}

window.onload = function(){ checkPermission(); }
