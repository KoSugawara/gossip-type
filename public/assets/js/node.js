
var uid = '';

var getuid = function()
{
    var $xhr = $.ajax({
        type : 'get',
        url  : '/gossiptype/conn/',
        async : false,
    });

    $xhr.done(function(data)
    {
        var info = $.parseJSON(data);
        uid = info.uid;
    });

    $xhr.fail(function(data)
    {
        alert('Fuck\'n Jap!');
    });
};
getuid();

var scroll = function()
{
    var y = $('#target').offset().top;
    $(window).scrollTop(y);
}
scroll();


var getMyHost = function()
{
    var host = location.host;
    var exp  = host.split(':');
    var port = '1234';

    var url = exp[0] + ':' + port;
    return url;
}

//Wellcome!
var s = io.connect(getMyHost());

//開始をnode.jsサーバへ通知
s.on('connect', function(){ s.emit('login', uid); });
s.on('disconnect', function(){ s.emit('disconnect', uid); });

/* ログイン情報 */
s.on('login_info', function(data)
{
    var users = data;

    $('#sidenav li i').removeClass('text-primary');
    $.each(users, function(key, val)
    {
        $('#sidenav #' + val).find('i').addClass('text-primary');
    });
});

/* 受信 */
s.on('S_to_C_message', function (data) 
{
    var res = data;//$.parseJSON(data);
    var _id = 'index';

    if(res.to)
    {
        if(res.to === res.owner && res.owner === uid)
            _id = uid;
        else if(res.to === uid && uid !== res.owner)
            _id = res.owner;
        else if(res.owner === uid && res.to !== uid)
            _id = res.to;
    }

    //コメント表示
    addMessage(res, _id);
    //下部にスクロール
    scroll();
    
    if(uid !== res.owner || (res.to && res.to === uid && res.owner !== uid))
        show(res.user, res.content);
});

var sendMessage = function()
{
    var msg = $('#message').val();
    $('#message').val('');

    var data = {
        v : msg,
        u : uid,
        r : _hash._r,
        i : _hash._id
    };

    s.emit('C_to_S_message', data);
}

var addMessage = function(data, _id)
{
    $('#div_' + _id).append(setMess(data.user, data.content, data.created));
}    
 
 $(document).on('submit', 'form', function()
 {
     sendMessage();
 });

 
var setMess = function(user, msg, time)
{
    var html  = '<div class="col-lg-12 clearfix">';
        html +=   '<div class="pull-left col-lg-2"><strong>'+ user +'</strong></div>';
        html +=   '<div class="pull-left col-lg-8">' + msg + '</div>';
        html +=   '<div class="pull-right col-lg-2">' + time + '</div>';
        html += '</div>';
        html += '<br class="clearfix"><hr class="clearfix">';
    return html;
}
