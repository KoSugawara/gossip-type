<?php
/**
 * The production database settings. These get merged with the global settings.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=localhost;dbname=fuel_prod',
			'username'   => 'fuel_app',
			'password'   => 'super_secret_password',
		),
	),
	'mongo' => array(
            'default' => array(
                'hostname'  => 'ds043022.mongolab.com',
                'port'      => 43022,
                'database' => 'gossip_type',
                'username' => 'gossipMaster',
                'password' => 'gossipadmin',
                'profiling' => true,
            )
        ),
    'redis' => array(
        'default' => array(
            'hostname'  => 'localhost',
            'port'      => 6379,
        ),
    ),
);
