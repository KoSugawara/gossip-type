<?php
/**
* Node.jsからのリクエストを受け取るコントローラ
**/

class Controller_Request extends Controller
{
    public function post_index()
    {
        $post = Input::post();
        $data = Model_Common::ContentInsert($post);

        return json_encode($data);
    }


    public function action_hoge($str = '')
    {
        echo \Auth::hash_password($str);
    }
}
