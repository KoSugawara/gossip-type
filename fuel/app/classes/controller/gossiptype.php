<?php
class Controller_Gossiptype extends Controller
{
    public function before()
    {
        parent::before();
        
        if(!Auth::check())
            Response::redirect('/auth/index/', 'refresh', 200);
    }

    /* Viewの共通化 */
    public function Layout($view)
    {
        $layout = View::forge('layout');
        $layout->set('content', $view);
        $layout->set_global('title', 'Gossip Type β0.01');
        $layout->set_global('users', Model_Common::createToYou());
        
        return $layout;
    }

    public function action_index()
    {
        Asset::js(['node.js', 'gossiptype.js'],[],'js_footer');
        $view = View::forge('gossiptype/index');

        return $this->Layout($view);
    }

    /* 5. viewからのコネクション受け取り */
    public function action_conn()
    {
        $post = Input::post();
        return json_encode(['uid' => Session::get('uid')]);
    }

    /* viewにレンダリングするデータ */
    public function action_getcontent($_id = 'index', $key = 'm')
    {
        if(!Input::is_ajax())
        {
            Response::redirect('/auth/logout/', 'refresh', 200);
            exit();
        }

        if(empty($_id) || empty($key))
            return json_encode(['user' => 'error', 'content' => 'bad request 400.', 'created' => date('Y-m-d H:i:s')]);
        
        $keylist = Model_Common::$collections;
        if(!array_key_exists($key, $keylist))
            return json_encode(['user' => 'deny', 'content' => 'lol.', 'created' => date('Y-m-d H:i:s')]);
        
        $data = Model_Common::GetContent($_id, $key);
        return json_encode($data);
    }


    /* 説明 */
    public function action_readme()
    {
        $view = View::forge('gossiptype/readme');
        return $view;//$this->Layout($view);
    }
}
