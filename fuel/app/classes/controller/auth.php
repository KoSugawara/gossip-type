<?php
/*
 * メモ
 * パスワードハッシュ化 : echo \Auth::hash_password('hoge');
 */
class Controller_Auth extends Controller
{
    public function before()
    {
        parent::before();
        
        $auth = \Auth::instance();
        if(\Auth::check() && !('logout' === Uri::segment(2)))
        {
            Response::redirect('/auth/index/', 'refresh', 200);
        }
    }

    // ユーザログイントップページ
    public function action_index()
    {        
        // セキュリティトークン発番
        $data['token_key'] = Config::get('security.csrf_token_key');
        $data['token']     = Security::fetch_token();

        return View::forge('auth/login', $data);
    }

    // ログインページ(action_index)からのID,pss送信
    public function action_login()
    {
        $flg = true;
        if(strtolower(Input::method()) !== 'post')
            $flg = false;
        if(Input::is_ajax())
            $flg = false;

        if(!$flg)
        {
            echo 'deny.';exit();return false;
        }

        $mail     = Input::post('mail');
        $password = Input::post('password');

        if((Auth::login($mail, $password)))
        {
            Response::redirect('/gossiptype/');
        }
        else
        {
            \Session::set_flash('error', 'Login faild.');
            Response::redirect('/auth/index/');
        }
        exit();
    }

    /*
     * ログアウト用メソッド
     * ログアウト後に
     */
    public function action_logout()
    {
        \Session::destroy();
        Response::redirect('/auth/index/');
    }
}

