<?php
//namespace Auth;

class Auth_Login_Gtauth extends \Auth\Auth_Login_Driver
{

    public static function _init()
    {
       \Config::load('gtauth', true, true, true);
    }

    protected $user = null;
    protected $config = array(
        'drivers' => array('group' => array('Gtauth')),
    );

    /*
     * ログインチェック
     * ログイン中の場合にはtrueを返す
     */
    public function perform_check()
    {
        $uid        = \Session::get('uid');
        $login_hash = \Session::get('key');

        // セッションのログイン情報があれば存在のチェック
        if(!empty($uid) AND !empty($login_hash))
        {
            $mongo = \Mongo_Db::instance();
            $user  = $mongo->where(['_id' => new MongoId($uid), 'login_hash' => $login_hash])
                           ->count('user');

            // ログイン中だった場合にtrueを返す
            if ($user)
                return true;
        }
        return false;
    }

    /*
     * ユーザ認証確認
     */
    public function validate_user($mail = '', $password = '')
    {
        if(empty($mail) OR empty($password))
            return false;
        
        $password = \Auth::hash_password($password);

        $mongo = \Mongo_Db::instance();
        $user  = $mongo->where(['mail' => $mail, 'pass' => $password])
                       ->get_one('user');

        // ログイン成功
        if(!is_null($user) && is_array($user))
        {
            // Last_login時間とセッション用hash更新
            $last_login = \Date::forge()->get_timestamp();
            $login_hash = sha1('unkochinkomanko'.$user['mail'].$last_login);

            $mongo->where(['_id' => $user['_id']])
                  ->update('user', ['last_login' => $last_login, 'login_hash' => $login_hash]);

            \Session::set('uid',  (string)$user['_id']);
            \Session::set('key',  $login_hash);
            \Session::set('name', $user['name']);

            return true;
        }

        return false;
    }

    /*
     * ログイン処理
     * 入力されたIDとパスワードのチェックを行う
     * \Auth::login($mail, $password, $contract_id);
     * 返り値: true or false
     */
    public function login($mail = '', $password = '')
    {
        if ($this->validate_user($mail, $password))
            return true;
        return false;
    }

    /*
     * logout
     * \Auth::logout();
     */
    public function logout()
    {
        \Session::destroy();
        return true;
    }

    public function get_user_id(){}
    public function get_groups(){}
    public function get_email(){}
    public function get_screen_name(){}
    // public function has_access(){}


} // End WwwauthClass
