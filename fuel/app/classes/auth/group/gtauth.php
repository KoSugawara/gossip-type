<?php
class Auth_Group_Gtauth extends \Auth\Auth_Group_Driver {

    // Aclの設定
    protected $config = array(
        'drivers' => array(
            'acl' => array('Gtauth')
        )
    );

    public function get_name($group = null) {
    }

    public function member($group, $user = null) {
        $auth = empty($user) ? \Auth::instance() : \Auth::instance($user[0]);
        $groups = $auth->get_groups();

        return in_array(array($this->id, $group), $groups);
    }
}
