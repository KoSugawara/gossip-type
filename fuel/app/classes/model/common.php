<?php

class Model_Common extends \Model
{
    public static $collections = [
        'm' => 'comment', //...room Collectionに親なし _id = index
        't' => 'toyou',   //...同上 _id = User.uid
        'c' => 'create',  //部屋を作れるようになったら考える
    ];

    /* 左のToYou作成 */
    public static function createToYou()
    {
        $mongo = \Mongo_Db::instance();
        $users = $mongo->get('user');
        
        return $users;
    }

    /* メッセージのインサート */
    public static function ContentInsert($data = [])
    {
        $user = self::GetUser($data['u']);
        $content = [
            'content' => $data['v'],
            'created' => date('Y-m-d H:i:s'),
            'owner'   => $data['u'],
        ];
        
        if($data['r'] === 't')
        {
            //個人宛て
            $content['to'] = $data['i'];
        }

        $collections = self::$collections;
        $mongo = \Mongo_Db::instance();
        $mongo->insert($collections[$data['r']], $content);

        $content['user'] = $user['name'];

        return $content;
    }

    /* uidからuserinfo取得 */
    public static function GetUser($uid = '')
    {
    	$mongo = \Mongo_Db::instance();
        $user  = $mongo->where(['_id' => new MongoId($uid)])
                       ->get_one('user');

        if(empty($user))
        	return false;

        return $user;
    }


    /* コメント一覧取得 */
    public static function GetContent($_id = '', $_key = '')
    {
        $data = [];

        if($_key === 'm')
            $data = self::getMain($_id, $_key);
        else if($_key === 't')
        {
            if($_id === Session::get('uid'))
                $data = self::getMe($_id, $_key);
            else
                $data = self::getToYou($_id, $_key);
        }
        else
            return [];

    	$users = self::createToYou();
        $user  = [];

        foreach ($users as $key => $val)
        {
        	$_id = (string)$val['_id'];
        	$user[$_id] = $val['name'];
        }

    	foreach ($data as $key => &$val)
    	{
    	    $val['user'] = $user[$val['owner']];	
    	}

    	return $data;
    }

    /* メインチャット */
    public static function getMain($_id = '', $_key = '')
    {
        $keys = self::$collections;
        $uid  = Session::get('uid');
        
        $mongo = \Mongo_Db::instance();
        $data = $mongo->order_by(['created' => 'asc'])
                      ->get($keys[$_key]);
        return $data;
    }

    /* 自分宛て */
    public static function getMe($_id = '', $_key = '')
    {
        $keys = self::$collections;
        $uid  = Session::get('uid');
        
        $mongo = \Mongo_Db::instance();
        $data = $mongo->order_by(['created' => 'asc'])
                      ->where(['to' => $_id, 'owner' => $_id])
                      ->get($keys[$_key]);
        return $data;
    }

    /* 個人宛て */
    public static function getToYou($_id = '', $_key = '')
    {
        $keys = self::$collections;
        $uid  = Session::get('uid');
        
        $mongo = \Mongo_Db::instance();

        //or_whereでいけなかった 。。。ムカつく
        $data1 = $mongo->order_by(['created' => 'asc'])
                       ->where(['to' => $_id, 'owner' => $uid])
                       ->get($keys[$_key]);

        $data2 = $mongo->order_by(['created' => 'asc'])
                       ->where(['to' => $uid, 'owner' => $_id])
                       ->get($keys[$_key]);

        //しかたないのでマージ
        $data = Arr::merge($data1, $data2);

        //もう一度sort
        $data = Arr::sort($data, 'created');
        return $data;
    }
}
