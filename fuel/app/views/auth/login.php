
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="robots" content="noindex,nofllow">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ログイン｜Gossip Type</title>
  <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
  <div class="container">
   <div class="row">
    <div class="col-xs-4 col-xs-offset-4 well" style="margin-top:200px;">
      
      <?php 
      $error = Session::get_flash('error');
      ?>
      
      <?php if (!empty($error)) : ?>
      <div class="alert alert-danger">
        <strong>Oh snap!</strong> <?=$error;?>
      </div>
      <?php endif; ?>
      
      <form action="/auth/login/" id="login_submit" role="form" accept-charset="utf-8" method="post">
        <div class="form-group">
          <p>E-mail</p>
          <input type="mail" name="mail" class="form-control" placeholder="hogehoge@foo.bar" id="mail" required>
        </div>
        <div class="form-group">
          <p>Password</p>
          <input type="password" name="password" class="form-control" placeholder="password" id="password" required>
        </div>
        <input type="hidden" name="<?=$token_key;?>" value="<?=$token;?>">
        <input type="submit" class="btn btn-primary btn-lg btn-block" value="ログイン">
      </form>
    </div>
<div>
  </div>

<script type="text/javascript" src="/assets/js/jquery.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>

</body>
</html>