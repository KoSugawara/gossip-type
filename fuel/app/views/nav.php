<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <!-- 左上 ヘッダー -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/gossiptype/">Gossip Type</a>
  </div>

  <!-- push通知 -->
  <div class="text-info pull-left col-md-offset-1" id="notify-permission" style="margin-top:7px;"></div>
  <button class="pull-left btn btn-link hide" id="push" onclick="requestPermission()">通知許可を求める</button>
  
  <!-- ヘッダーメニュー -->
  <ul class="clearfix nav navbar-right top-nav pull-right">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?=Session::get('name');?> <b class="caret"></b> </a>
      <ul class="dropdown-menu">
        <li><a href="#"><i class="fa fa-fw fa-user"></i> Profile</a></li>
        <li><a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a></li>
        <li class="divider"></li>
        <li><a href="/auth/logout/"><i class="fa fa-fw fa-power-off"></i> Log Out</a></li>
      </ul>
    </li>
  </ul>
    
  <!-- 左のメニュー -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav" id="sidenav">
            <!--<li class=""><a href="#readme"><i class="fa fa-fw fa-file"></i> ReadMe</a></li>-->
            <li class="active" id="index"><a href="#index=m"><i class="fa fa-fw fa-edit"></i> Room</a></li>
            
            <?php if(!empty($users)) : ?>
            
              <?php foreach($users as $key => $val) : ?>
                <li id="<?=(string)$val['_id'];?>"><a href="#<?=(string)$val['_id'];?>=t"><i class="fa fa-fw fa-user "></i> <?=$val['name'];?></a></li>
              <?php endforeach; ?>
              
            <?php endif; ?>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>